import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { 
    path: 'sistemas' ,
    loadChildren: () => import('./sistemas/sistemas.module').then(m => m.SistemasModule),
  },
  { 
    path: 'auth' ,
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
  },
  { 
    path: '**', 
    redirectTo: 'auth/login'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
