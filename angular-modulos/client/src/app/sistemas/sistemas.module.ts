import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SistemasRoutingModule } from './sistemas-routing.module';

import { HvListComponent } from './hv/hv-list/hv-list.component';
import { HvEditComponent } from './hv/hv-edit/hv-edit.component';
import { BitacoraEditComponent } from './bitacora/bitacora-edit/bitacora-edit.component';
import { BitacoraListComponent } from './bitacora/bitacora-list/bitacora-list.component';


@NgModule({
  declarations: [
    HvListComponent,
    HvEditComponent,
    BitacoraEditComponent,
    BitacoraListComponent
  ],
  imports: [
    CommonModule,
    SistemasRoutingModule
  ]
})
export class SistemasModule { }
