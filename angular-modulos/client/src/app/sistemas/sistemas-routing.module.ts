import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BitacoraEditComponent } from './bitacora/bitacora-edit/bitacora-edit.component';

import { BitacoraListComponent } from './bitacora/bitacora-list/bitacora-list.component';
import { HvEditComponent } from './hv/hv-edit/hv-edit.component';
import { HvListComponent } from './hv/hv-list/hv-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'bitacora/listar', component: BitacoraListComponent },
      { path: 'bitacora/guardar', component: BitacoraEditComponent },
      { path: 'hv/listar', component: HvListComponent },
      { path: 'hv/guardar', component: HvEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SistemasRoutingModule { }
