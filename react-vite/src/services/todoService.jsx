const API_URL = import.meta.env.VITE_API_URL;

export async function getHttpData(url) {
  if (!url) throw new Error("Not pased url");
  return fetch(`${API_URL}/${url}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then(async (response) => {
    if (!response.ok) {
      return response.text().then((text) => {
        throw new Error(text);
      });
    }
    return response.json();
  });
}
