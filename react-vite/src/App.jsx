import "./App.css";
import { Container, Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";
import TodoCounter from "./components/TodoCounter";
import { getHttpData } from "./services/todoService";

function App() {
  const [todos, setTodos] = useState([]);
  const [todosEnd, setTodosEnd] = useState(0);

  useEffect(() => {
    getHttpData("todos")
      .then((res) => {
        const data = res.data;
        setTodos(data);
        setTodosEnd(data.filter((t) => t.attributes.estado == true).length);
      })
      .catch((e) => alert(e.message));
  }, []);

  return (
    <div className="App">
      <Container>
        <Row className="justify-content-md-center">
          <Col className="text-center">
            <TodoCounter todosEnd={todosEnd} totalTodos={todos.length} />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
