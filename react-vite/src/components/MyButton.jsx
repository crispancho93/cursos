import React from "react";
import { Button } from "react-bootstrap";
import PropTypes from "prop-types";

const MyButton = (props) => {
  return (
    <>
      <div
        style={{
          borderStyle: "groove",
          display: "inline-block",
          padding: 5,
        }}
      >
        <Button variant={props.variant}>{props.text}</Button>
        {props.children}
      </div>
    </>
  );
};

MyButton.defaultProps = {
  variant: "primary",
  text: "Text",
};

MyButton.propTypes = {
  children: PropTypes.element,
  variant: PropTypes.string,
};

export default MyButton;
