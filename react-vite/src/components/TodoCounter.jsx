import React from "react";
import PropTypes from "prop-types";

/**
 * 
 * @param {*} param0 
 * @returns 
 */
const TodoCounter = ({ todosEnd, totalTodos }) => {
  return (
    <h3>
      Has completado {todosEnd} de {totalTodos} TODOS
    </h3>
  );
};

TodoCounter.defaultProps = {
  todosEnd: 0,
  totalTodos: 0,
};

TodoCounter.propTypes = {
  todosEnd: PropTypes.number,
  totalTodos: PropTypes.number,
};

export default TodoCounter;
