import { Component, OnInit } from '@angular/core';
import { RickandmortyService } from 'src/app/services/rickandmorty/rickandmorty.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {

  public characters: Array<any> = [];

  constructor(private _rickServ: RickandmortyService) {}

  ngOnInit(): void {
    const data = this._rickServ.getAll().subscribe((d:any) => {
      this.characters = d.results;
      console.log(this.characters);
    });
  }
}
