import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RickandmortyService {

  private _API: string = 'https://rickandmortyapi.com/api/character';

  constructor(private _http: HttpClient) { }

  getAll() {
    const data = this._http.get(this._API);
    return data;
  }
}
