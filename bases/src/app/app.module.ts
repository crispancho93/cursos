import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ListComponent } from './rickandmorty/list/list.component';
import { ListIdComponent } from './rickandmorty/list-id/list-id.component';
import { RickandmortyService } from './services/rickandmorty/rickandmorty.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ListIdComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    RickandmortyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
