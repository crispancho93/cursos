using PlatformService.Models;

namespace PlatformService.Data;

public class PlatformRepo : IPlatformRepo
{
    private readonly AppDbContext _context;

    public PlatformRepo(AppDbContext context) => _context = context;

    public int CreatePlatform(Platform platform)
    {
        var platformSave = _context.Platforms.Add(platform);
        _context.SaveChanges();
        return platform.Id;
    }

    public int UpdatePlatform(Platform platform, int id)
    {
        var found = _context.Platforms.Find(id);
        if (found is null) throw new Exception("Not Found Platform");

        found.Name = platform.Name??found.Name;
        found.Publisher = platform.Publisher??found.Publisher;
        found.Cost = platform.Cost??found.Cost;

        _context.Update(found);
        return _context.SaveChanges();
    }

    public Platform GetPlatformById(int id)
    {
        return _context.Platforms.Find(id);
    }

    public IEnumerable<Platform> GetAllPlatforms()
    {
        return _context.Platforms.ToList();
    }

    public int DeletePlatform(int id)
    {
        var delete = _context.Platforms.Find(id);
        if (delete is null) return 0;

        _context.Remove(delete);
        return _context.SaveChanges();
    }
}