using PlatformService.Models;

namespace PlatformService.Data;

public interface IPlatformRepo
{
    int CreatePlatform(Platform platform);
    int UpdatePlatform(Platform platform, int id);
    Platform GetPlatformById(int id);
    IEnumerable<Platform> GetAllPlatforms();
    int DeletePlatform(int id);
}