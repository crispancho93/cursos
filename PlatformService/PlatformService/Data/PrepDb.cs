namespace PlatformService.Data;

public static class PrepDb
{
    public static void PrepPopulation(this IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.CreateAsyncScope();
        var context = scope.ServiceProvider.GetService<AppDbContext>();

        if (!context.Platforms.Any()) 
        {
            Console.WriteLine("--> Seeding data...");

            context.Platforms.AddRange(
                new Models.Platform { Name = "Linux", Publisher = "1980", Cost = "Free" },
                new Models.Platform { Name = "Docker", Publisher = "2019", Cost = "Free" },
                new Models.Platform { Name = "Windows Server", Publisher = "2000", Cost = "US" },
                new Models.Platform { Name = "Centos Server", Publisher = "1980", Cost = "Free" },
                new Models.Platform { Name = "Linux Mint Vanessa", Publisher = "2022", Cost = "Free" }
            );

            context.SaveChanges();
        }
        else
        {
            Console.WriteLine("--> We already have data");
        }
    }
}