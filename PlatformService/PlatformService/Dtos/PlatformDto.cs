using System.ComponentModel.DataAnnotations;

namespace PlatformService.Dtos;

public class PlatformDto
{
    public int Id { get; set; }
    
    [Required]
    public string Name { get; set; }

    [Required]
    public string Publisher { get; set; }

    public string Cost { get; set; }
}