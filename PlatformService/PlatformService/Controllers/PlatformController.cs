using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PlatformService.Data;
using PlatformService.Dtos;
using PlatformService.Models;

namespace PlatformService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class PlatformController : ControllerBase
{
    private readonly ILogger<PlatformController> _logger;
    private readonly IMapper _mapper;
    private readonly IPlatformRepo _platformRepo;

    public PlatformController(ILogger<PlatformController> logger, IMapper mapper, IPlatformRepo platformRepo)
    {
        _logger = logger;
        _mapper = mapper;
        _platformRepo = platformRepo;
    }

    [HttpPost]
    public ActionResult<PlatformDto> CreatePlatform(PlatformDto platformDto)
    {
        try
        {
            _logger.LogInformation("--> CreatePlatform");
            if (!ModelState.IsValid) return BadRequest();

            var platformModel = _mapper.Map<Platform>(platformDto);
            var insertedId = _platformRepo.CreatePlatform(platformModel);

            if (insertedId is 0) return BadRequest("Not save platform");

            return Ok(new { insertedId });
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPut("{Id}")]
    public ActionResult<PlatformDto> UpdatePlatform(PlatformDto platformDto, int id)
    {
        try
        {
            _logger.LogInformation("--> UpdatePlatform");
            if (!ModelState.IsValid) return BadRequest();

            var platformModel = _mapper.Map<Platform>(platformDto);
            platformModel.Id = id;

            var affected = _platformRepo.UpdatePlatform(platformModel, id);
            if (affected is 0) return NotFound();

            return Ok();
        }
        catch (Exception ex)
        {
            return BadRequest(new { msg = ex.Message });
        }
    }


    [HttpGet("{id}", Name = "GetPlatformById")]
    public ActionResult<PlatformDto> GetPlatformById(int id)
    {
        try
        {
            _logger.LogInformation("--> GetPlatformById");
            var platformItem = _platformRepo.GetPlatformById(id);

            if (platformItem is null) return NotFound();
            return Ok(_mapper.Map<PlatformDto>(platformItem));
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet]
    public ActionResult<IEnumerable<PlatformDto>> GetPlatforms()
    {
        try
        {
            _logger.LogInformation("--> GetAllPlatforms");
            var platformItem = _platformRepo.GetAllPlatforms();
            return Ok(_mapper.Map<IEnumerable<PlatformDto>>(platformItem));
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }
}
