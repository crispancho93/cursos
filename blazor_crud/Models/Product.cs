namespace blazor_crud.Models;

public class Product
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Reference { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public string? Description { get; set; }
}

public class ProductSchema
{
    public int Id { get; set; }
    public ProductAttributes Attributes { get; set; } = new ProductAttributes();
}

public class ProductAttributes
{
    public string? Name { get; set; }
    public string? Reference { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public string? Description { get; set; }
}

public class ProductSchemaList
{
    public IEnumerable<ProductSchema>? Data { get; set; }
}

public class ProductSchemaItem
{
    public ProductSchema? Data { get; set; }
}