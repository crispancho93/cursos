using System.Net.Http.Json;
using blazor_crud.Models;
using Newtonsoft.Json;

namespace blazor_crud.Services.Products;

public class ProductService : IProductService
{
    private readonly HttpClient _http;
    private readonly IConfiguration _config;
    public IEnumerable<Product> Products { get; set; } = Enumerable.Empty<Product>();

    public ProductService(HttpClient http, IConfiguration config)
    {
        _http = http;
        _config = config;
    }

    public async Task GetActiveProducts()
    {
        var schema = await _http.GetFromJsonAsync<ProductSchemaList>($"{_config["API_URL"]}/products");

        if (schema != null)
        {
            var productsSchema = schema?.Data;

            Products = from p in productsSchema
                select new Product
                {
                    Id = p.Id,
                    Name = p.Attributes.Name,
                    Reference = p.Attributes.Reference,
                    Price = p.Attributes.Price,
                    Quantity = p.Attributes.Quantity,
                    Description = p.Attributes.Description,
                };
        }
    }

    public async Task<Product> GetProductById(int id)
    {
        var schema = await _http.GetFromJsonAsync<ProductSchemaItem>($"{_config["API_URL"]}/products/{id}");
        var productSchema = schema?.Data;

        if (productSchema is not null)
        {
            return new Product
            {
                Id = productSchema.Id,
                Name = productSchema.Attributes.Name,
                Reference = productSchema.Attributes.Reference,
                Price = productSchema.Attributes.Price,
                Quantity = productSchema.Attributes.Quantity,
                Description = productSchema.Attributes.Description,
            };
        }
        else
        {
            return new Product();
        }
    }
}