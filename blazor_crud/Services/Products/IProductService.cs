using blazor_crud.Models;

namespace blazor_crud.Services.Products;

public interface IProductService
{
    public IEnumerable<Product> Products { get; set; }
    public Task GetActiveProducts();
    public Task<Product> GetProductById(int id);
}