package annotation;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component // inyecta en el contenedor
//@Scope("prototype") // prototype || singleton
public class Comercial implements IEmpleado {
	
	@Autowired // recupera del contenedor
	@Qualifier("informeGerencia") // cuando más de una clase implementan la misma interfaz
	private IInforme informe;
	

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Tareas comercial";
	}

	@Override
	public String getInformes() {
		// TODO Auto-generated method stub
		return informe.getInformeBase() +" Informes comercial";
	}
	
	@PostConstruct // solo funciona con singleton
	public void ejecutaAntesCreacionBean() {
		System.out.println("Se crea el bean");
	}
	
	@PreDestroy // solo funciona con singleton
	public void ejecutaDespuesCreacionBean() {
		System.out.println("Se destruye el bean");
	}
}
