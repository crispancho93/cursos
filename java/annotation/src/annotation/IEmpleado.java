package annotation;

public interface IEmpleado {
	
	public String getTareas();
	public String getInformes();
}
