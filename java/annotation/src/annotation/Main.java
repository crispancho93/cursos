package annotation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		// Start context - load xml beans
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		IEmpleado comercial = context.getBean("comercial", IEmpleado.class);
		System.out.println(comercial.getInformes());
		
		IEmpleado comercial2 = context.getBean("comercial", IEmpleado.class);
		System.out.println(comercial2.getInformes());
		
		if (comercial == comercial2) {
			System.out.println("Iguales");
			System.out.println(comercial);
			System.out.println(comercial2);
		} else {
			System.out.println("Desiguales");
			System.out.println(comercial);
			System.out.println(comercial2);
		}
		
		// Close context
		context.close();
	}
}
