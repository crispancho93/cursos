package ioc;

public class DirectorEmpleado implements IEmpleados {
	
	private IInformes informe;
	
	public DirectorEmpleado(IInformes informe) {
		this.informe = informe;
	}

	@Override
	public String getTareas() {
		return "Tarea DirectorEmpleado";
	}

	@Override
	public String getInforme() {
		return informe.getInforme() + " - DirectorEmpleado";
	}
}
