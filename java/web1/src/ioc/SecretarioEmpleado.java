package ioc;

public class SecretarioEmpleado implements IEmpleados {
	
	private IInformes informe;
	
	public SecretarioEmpleado(IInformes informe) {
		this.informe = informe;
	}

	@Override
	public String getTareas() {
		return "Tarea SecretarioEmpleado";
	}
	
	@Override
	public String getInforme() {
		return informe.getInforme() + " - SecretarioEmpleado";
	}
}
