package ioc;

public class JefeEmpleado implements IEmpleados {
	
	private IInformes informe;
	
	public JefeEmpleado(IInformes informe) {
		this.informe = informe;
	}
	
	@Override
	public String getTareas() {
		return "Tarea JefeEmpleado";
	}
	
	@Override
	public String getInforme() {
		return informe.getInforme() + " - JefeEmpleado";
	}
}
