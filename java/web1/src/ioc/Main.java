package ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		/*IEmpleados jefe = new DirectorEmpleado();
		System.out.println(jefe.getTareas());*/
		
		// Spring base
		
		// Start context - load xml beans
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		
		// Get beans
		IEmpleados director = context.getBean("jefeEmpleado", IEmpleados.class);
		System.out.println(director.getInforme());
		
		// Close context
		context.close();
	}

}
