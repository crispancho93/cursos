package annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("annotation")
public class Main {

	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Main.class);
		
		IEmpleado comercial = context.getBean("comercial", IEmpleado.class);
		System.out.println(comercial.getInformes());
		
		IEmpleado comercial2 = context.getBean("comercial", IEmpleado.class);
		System.out.println(comercial2.getInformes());
		
		if (comercial == comercial2) {
			System.out.println("Iguales");
			System.out.println(comercial);
			System.out.println(comercial2);
		} else {
			System.out.println("Desiguales");
			System.out.println(comercial);
			System.out.println(comercial2);
		}
		
		// Close context
		context.close();
	}

}
