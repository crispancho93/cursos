﻿// Global
using System.Reflection;

var _query = new LinqQueries();

// Operadoradores básicos

// Where

// Reto 1
// ImprimirValores(_query.LibrosDespuesDeCiertoAnio("method", 2000));

// Reto 2
// ImprimirValores(_query.LibrosMasDe250Paginas("query", 250, "in Action"));

// All - Verifica que una condición o varias condiciones se cumplan en todos los elementos
// Console.WriteLine($"Todos los libros tienen Status? - {_query.LibrosConValorEnStatus("method")}");

// Any - Verifica que una condición o varias condiciones se cumplan en almenos un elemento
// Console.WriteLine($"Algún libro fue publicado en el 2005? - {_query.AlgunLibroPublicadoEn2005("method", 2005)}");

// Contains - Verifica si existe o no un elemento en espesifico
// ImprimirValores(_query.LibrosPorCategoria("method", "microsoft .net"));

// OrderBy
// ImprimirValores(_query.LibrosOrdenadoXCategoria("query", "java"));

// OrderBy Desc
// ImprimirValores(_query.LibrosDescXPaginas("query", 300));

// Take operator
// ImprimirValores(_query.TresPrimerosLibrosXCategoria("query", "java", 3));

// Skip operator
// ImprimirValores(_query.UltimosDosLibrosXCantidadPaginas("method"));

// Select operator
// ImprimirValores<BookLite>(_query.TresPrimerosLibrosSelect1("query"));

// Count operator
// Console.WriteLine(_query.LibrosEntre200Y500Paginas("method"));

// Operador Min
// Console.WriteLine(_query.MenorFechaOperadorMin("method").ToString());

// Operador Max
// System.Console.WriteLine(_query.NumeroDePaginasLibroMayor("method"));

// Operador MinBy
// var list = new List<Book>();
// list.Add(_query.LibroConMenosPaginas("method"));
// ImprimirValores(list);

// Operador MinBy
// var list = new List<Book>();
// list.Add(_query.FechaPublicacionMasReciente("method"));
// ImprimirValores(list);

// Operador Sum
// System.Console.WriteLine(_query.SumaPaginas1("query"));

// Operator aggregate
Console.WriteLine(_query.TitulosLibrosMayorAl2015());

void ImprimirValores<T>(IEnumerable<T> books)
{
    Console.WriteLine("{0, -60} {1, 15} {2, 15}\n", "Titulo", "N. Paginas", "Fecha");
    foreach (var book in books)
    {
        var title = book.GetType().GetProperty("Title").GetValue(book, null);
        var pageCount = book.GetType().GetProperty("PageCount").GetValue(book, null);
        var publishedDate = Convert.ToDateTime(book.GetType().GetProperty("PublishedDate")?.GetValue(book, null));

        Console.WriteLine("{0, -60} {1, 15} {2, 15}", title, pageCount, publishedDate.ToShortDateString());
    }
}