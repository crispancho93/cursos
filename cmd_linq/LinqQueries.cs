using System.Text.Json;

public class LinqQueries
{
    private readonly List<Book> _booksCollection = new List<Book>();

    public LinqQueries()
    {
        using (var reader = new StreamReader("books.json"))
        {
            var json = reader.ReadToEnd();
            _booksCollection = JsonSerializer.Deserialize<List<Book>>(json, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? Enumerable.Empty<Book>().ToList();
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Book> GetAllBooks()
    {
        return _booksCollection;
    }

    /// <summary>
    /// Reto 1
    /// Retornar los libros que fueron publicados después de cierto año
    /// </summary>
    /// <param name="method"></param>
    /// <param name="year"></param>
    /// <returns></returns>
    public IEnumerable<Book> LibrosDespuesDeCiertoAnio(string method, int year)
    {
        if (method is "method")
        {
            // Extension method
            return _booksCollection.Where(b => b.PublishedDate.Year > year).OrderByDescending(b => b.PublishedDate);
        }
        else if (method is "query")
        {
            // Query expresion
            return from b in _booksCollection
                   where b.PublishedDate.Year > year
                   orderby b.PublishedDate descending
                   select b;
        }
        else
        {
            return Enumerable.Empty<Book>();
        }
    }

    /// <summary>
    /// Reto 2
    /// Libros que tengan más de 250 páginas y el título contenga la palabra in Action
    /// </summary>
    /// <param name="pages"></param>
    /// <param name="title"></param>
    /// <returns></returns>
    public IEnumerable<Book> LibrosMasDe250Paginas(string method, int pages, string title)
    {
        if (method is "method")
        {
            return _booksCollection.Where(b => b.PageCount > pages && b.Title.Contains(title));
        }
        else if (method is "query")
        {
            return from b in _booksCollection
                   where b.PageCount > pages && b.Title.Contains(title)
                   select b;
        }
        else
        {
            return Enumerable.Empty<Book>();
        }
    }

    /// <summary>
    /// Verifica que una condición o varias condiciones se cumplan en todos los elementos
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public bool LibrosConValorEnStatus(string method)
    {
        if (method is "method")
        {
            return _booksCollection.All(b => b.Status != string.Empty);
        }
        else if (method is "query")
        {
            return (from b in _booksCollection select b).All(x => x.Status != string.Empty);
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Any - Verifica que una condición o varias condiciones se cumplan en almenos un elemento
    /// </summary>
    /// <param name="method"></param>
    /// <param name="year"></param>
    /// <returns></returns>
    public bool AlgunLibroPublicadoEn2005(string method, int year)
    {
        if (method is "method")
        {
            return _booksCollection.Any(b => b.PublishedDate.Year == year);
        }
        else if (method is "query")
        {
            return (from b in _booksCollection select b).Any(b => b.PublishedDate.Year == year);
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Retornar los elementos que pertenezcan a la categoria de Python
    /// </summary>
    /// <param name="method"></param>
    /// <param name="categoria"></param>
    /// <returns></returns>
    public IEnumerable<Book> LibrosPorCategoria(string method, string categoria)
    {
        if (method is "method")
        {
            return _booksCollection.Where(b => b.Categories.Contains(categoria, StringComparer.CurrentCultureIgnoreCase));
        }
        else if (method is "query")
        {
            return from b in _booksCollection
                   where b.Categories.Contains(categoria, StringComparer.CurrentCultureIgnoreCase)
                   select b;
        }
        else
        {
            return Enumerable.Empty<Book>();
        }
    }

    /// <summary>
    /// Libros por categoria ordenados
    /// </summary>
    /// <param name="method"></param>
    /// <param name="categoria"></param>
    /// <returns></returns>
    public IEnumerable<Book> LibrosOrdenadoXCategoria(string method, string categoria)
    {
        if (method is "method")
        {
            return _booksCollection.Where(b => b.Categories.Contains(categoria, StringComparer.CurrentCultureIgnoreCase)).OrderBy(b => b.Title);
        }
        else if (method is "query")
        {
            return from b in _booksCollection
                   where b.Categories.Contains(categoria)
                   orderby b.Title
                   select b;
        }
        else
        {
            return Enumerable.Empty<Book>();
        }
    }

    /// <summary>
    /// Utilizando el operador orderbydesc retorna los libros que tengan mas de 450
    /// paginas, ordenados por numero de paginas desc
    /// </summary>
    /// <param name="method"></param>
    /// <param name="page"></param>
    /// <returns></returns>
    public IEnumerable<Book> LibrosDescXPaginas(string method, int page)
    {
        if (method is "method")
        {
            return _booksCollection.Where(b => b.PageCount > page).OrderByDescending(b => b.PageCount);
        }
        else if (method is "query")
        {
            return from b in _booksCollection
                   where b.PageCount > page
                   orderby b.PageCount descending
                   select b;
        }
        else
        {
            return Enumerable.Empty<Book>();
        }
    }

    /// <summary>
    /// Utilizando el operador Take selecciona los 3 primeros libros con fecha de publicación
    /// más reciente que estén categorizados
    /// </summary>
    /// <param name="method"></param>
    /// <param name="categoria"></param>
    /// <param name="limit"></param>
    /// <returns></returns>
    public IEnumerable<Book> TresPrimerosLibrosXCategoria(string method, string categoria, int limit)
    {
        if (method is "method")
        {
            return _booksCollection
                .Where(b => b.Categories.Contains(categoria, StringComparer.CurrentCultureIgnoreCase))
                .OrderByDescending(b => b.PublishedDate)
                .Take(limit);
        }
        else if (method is "query")
        {
            return (
                from b in _booksCollection
                where b.Categories.Contains(categoria, StringComparer.CurrentCultureIgnoreCase)
                orderby b.PublishedDate
                select b
            ).TakeLast(limit);
        }
        else
        {
            return Enumerable.Empty<Book>();
        }
    }

    /// <summary>
    /// Utilizando el operado Skip, retorna el tercer y cuarto libro
    /// de los que tengan más de 400 paginas
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public IEnumerable<Book> UltimosDosLibrosXCantidadPaginas(string method)
    {
        if (method is "method")
        {
            return _booksCollection.Where(b => b.PageCount > 400).Take(4).Skip(2);
        }
        else if (method is "query")
        {
            return (
                from b in _booksCollection
                where b.PageCount > 400
                select b
            ).Take(400).Skip(2);
        }
        else
        {
            return Enumerable.Empty<Book>();
        }
    }

    /// <summary>
    /// Utilizando el operador select seleccion el título y el número de páginas
    /// de los primeros tres libros de la coleccion
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public IEnumerable<BookLite> TresPrimerosLibrosSelect1(string method)
    {
        if (method is "method")
        {
            return _booksCollection.Take(3).Select(b => new BookLite { Title = b.Title, PageCount = b.PageCount });
        }
        else if (method is "query")
        {
            return (
                from b in _booksCollection
                select new BookLite
                {
                    Title = b.Title,
                    PageCount = b.PageCount
                }
            ).Take(3);
        }
        else
        {
            return Enumerable.Empty<BookLite>();
        }
    }

    /// <summary>
    /// Utilizanod el operador Count, retorna el número de libros que contengan
    /// entre 200 y 500 páginas
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public int LibrosEntre200Y500Paginas(string method)
    {
        if (method is "method")
        {
            return _booksCollection.Count(b => b.PageCount >= 200 && b.PageCount <= 500);
        }
        else if (method is "query")
        {
            return (
                from b in _booksCollection
                where b.PageCount >= 200 && b.PageCount <= 200
                select b
            ).Count();
        }
        else
        {
            return 0;
        }
    }

    /// <summary>
    /// Utilizando el operador min, retorna la menos fecha
    /// de la publicación de libros
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public DateTime MenorFechaOperadorMin(string method)
    {
        if (method is "method")
        {
            return _booksCollection.Min(b => b.PublishedDate);
        }
        else
        {
            return (from b in _booksCollection select b.PublishedDate).Min();
        }
    }

    /// <summary>
    /// Utilizando el operador Max retorna el libro con más paginas
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public int NumeroDePaginasLibroMayor(string method)
    {
        if (method is "method")
        {
            return _booksCollection.Max(b => b.PageCount);
        }
        else
        {
            return (from b in _booksCollection select b.PageCount).Max();
        }
    }

    /// <summary>
    /// Retorna el libro con la menor cantidad de páginas mayor a cero
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public Book LibroConMenosPaginas(string method)
    {
        if (method is "method")
        {
            return _booksCollection.Where(b => b.PageCount > 0).MinBy(b => b.PageCount);
        }
        else
        {
            return (from b in _booksCollection where b.PageCount > 0 select b).MinBy(b => b.PageCount);
        }
    }

    /// <summary>
    /// Retorna el libro con la fecha de publicación más reciente
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public Book FechaPublicacionMasReciente(string method)
    {
        if (method is "method")
        {
            return _booksCollection.MaxBy(b => b.PublishedDate);
        }
        else
        {
            return (from b in _booksCollection select b).MaxBy(b => b.PublishedDate);
        }
    }

    /// <summary>
    /// Retorna la suma de páginas, de todos los libros que tengan entre 0 y 500
    /// </summary>
    /// <param name="method"></param>
    /// <returns></returns>
    public int SumaPaginas1(string method)
    {
        if (method is "method")
        {
            return _booksCollection.Where(b => b.PageCount >= 0 && b.PageCount <= 500).Sum(b => b.PageCount);
        }
        else
        {
            return (
                from b in _booksCollection
                where b.PageCount > 0 && b.PageCount < 500
                select b.PageCount
            ).Sum();
        }
    }

    /// <summary>
    /// Retorna el título de los libros que tienen fecha de publicación posterior al 2015
    /// </summary>
    /// <returns></returns>
    public string TitulosLibrosMayorAl2015()
    {
        return _booksCollection
            .Where(b => b.PublishedDate.Year > 2015)
            .Aggregate("", (TitulosLibros, next) =>
            {
                if (TitulosLibros != string.Empty)
                    TitulosLibros += $" - {next.Title}";
                else
                    TitulosLibros += next.Title;

                return TitulosLibros;
            });
    }
}